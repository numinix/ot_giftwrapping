<?php
// Constants related to Gift Wrap 2.7 or greater
define('GIFTWRAP_IMAGE_HEIGHT', 30);
define('GIFTWRAP_IMAGE_WIDTH', 30); 

// If Wrap Selection option is "Descriptions", edit the list 
// $wrap_selections in 
// includes/languages/YOUR LANGUAGE/extra_definitions/giftwrap.php

// If Wrap Selection option is "Images", put images in 
// CART_HOME/GIFTWRAP_IMAGE_DIR.  They will be autodetected.
// Defaults to images/giftwrap 
define('GIFTWRAP_IMAGE_DIR', "../" . DIR_WS_IMAGES . 'giftwrap/'); 

?>
