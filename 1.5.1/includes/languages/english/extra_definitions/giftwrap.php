<?php
  // Special definitions for Gift Wrap Contribution
  define('GIFT_WRAP_HEADING', 'Gift Wrapping Options'); 
  define('GIFT_WRAP_SELECTIONS', 'Wrapping Paper Options'); 
  define('GIFT_WRAP_SUMMARY_HEADING', 'Items to be Gift Wrapped'); 
  define('GIFT_WRAP_CHECKOFF', 'Wrap?'); 
  define('GIFT_WRAP_NO_TEXT', 'No gift wrapping selected'); 
  define('GIFT_WRAP_EXPLAIN_DETAILS', 'Our gift wrapping policies are: ... ');
  define('GIFT_WRAP_EXPLAIN_LINK', 'Gift Wrapping Policies'); 
  define('GIFT_WRAP_NA', 'N/A'); 

  // Strings in the email
  define('EMAIL_TEXT_GIFTWRAP', 'Wrapped: ');
  define('GIFT_WRAP_PAPER_SELECTED', 'Gift wrapping selected: '); 
  define('GIFT_WRAP_IMAGE_UNAVAILABLE', 'Gift wrap image unavailable; name is: '); 

  // Use this list if Wrap Selection option is "Descriptions"
  $wrap_selections = array('Holiday', 'Birthday', 'Everyday'); 

