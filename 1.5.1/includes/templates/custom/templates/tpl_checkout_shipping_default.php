<?php
/**
 * Page Template
 *
 * Loaded automatically by index.php?main_page=checkout_shipping.<br />
 * Displays allowed shipping modules for selection by customer.
 *
 * @package templateSystem
 * @copyright Copyright 2003-2009 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 * @version $Id: tpl_checkout_shipping_default.php 14807 2009-11-13 17:22:47Z drbyte $
 */
?>
<div class="centerColumn" id="checkoutShipping">

<?php echo zen_draw_form('checkout_address', zen_href_link(FILENAME_CHECKOUT_SHIPPING, '', 'SSL')) . zen_draw_hidden_field('action', 'process'); ?>

<h1 id="checkoutShippingHeading"><?php echo HEADING_TITLE; ?></h1>
<?php if ($messageStack->size('checkout_shipping') > 0) echo $messageStack->output('checkout_shipping'); ?>

<h2 id="checkoutShippingHeadingAddress"><?php echo TITLE_SHIPPING_ADDRESS; ?></h2>

<div id="checkoutShipto" class="floatingBox back">
<?php if ($displayAddressEdit) { ?>
<div class="buttonRow forward"><?php echo '<a href="' . $editShippingButtonLink . '">' . zen_image_button(BUTTON_IMAGE_CHANGE_ADDRESS, BUTTON_CHANGE_ADDRESS_ALT) . '</a>'; ?></div>
<?php } ?>
<address class=""><?php echo zen_address_label($_SESSION['customer_id'], $_SESSION['sendto'], true, ' ', '<br />'); ?></address>
</div>
<div class="floatingBox important forward"><?php echo TEXT_CHOOSE_SHIPPING_DESTINATION; ?></div>
<br class="clearBoth" />

<?php
  if (zen_count_shipping_modules() > 0) {
?>

<h2 id="checkoutShippingHeadingMethod"><?php echo TABLE_HEADING_SHIPPING_METHOD; ?></h2>

<?php
    if (sizeof($quotes) > 1 && sizeof($quotes[0]) > 1) {
?>

<div id="checkoutShippingContentChoose" class="important"><?php echo TEXT_CHOOSE_SHIPPING_METHOD; ?></div>

<?php
    } elseif ($free_shipping == false) {
?>
<div id="checkoutShippingContentChoose" class="important"><?php echo TEXT_ENTER_SHIPPING_INFORMATION; ?></div>

<?php
    }
?>
<?php
    if ($free_shipping == true) {
?>
<div id="freeShip" class="important" ><?php echo FREE_SHIPPING_TITLE; ?>&nbsp;<?php echo $quotes[$i]['icon']; ?></div>
<div id="defaultSelected"><?php echo sprintf(FREE_SHIPPING_DESCRIPTION, $currencies->format(MODULE_ORDER_TOTAL_SHIPPING_FREE_SHIPPING_OVER)) . zen_draw_hidden_field('shipping', 'free_free'); ?></div>

<?php
    } else {
      $radio_buttons = 0;
      for ($i=0, $n=sizeof($quotes); $i<$n; $i++) {
      // bof: field set
// allows FedEx to work comment comment out Standard and Uncomment FedEx
//      if ($quotes[$i]['id'] != '' || $quotes[$i]['module'] != '') { // FedEx
      if ($quotes[$i]['module'] != '') { // Standard
?>
<fieldset>
<legend><?php echo $quotes[$i]['module']; ?>&nbsp;<?php if (isset($quotes[$i]['icon']) && zen_not_null($quotes[$i]['icon'])) { echo $quotes[$i]['icon']; } ?></legend>

<?php
        if (isset($quotes[$i]['error'])) {
?>
      <div><?php echo $quotes[$i]['error']; ?></div>
<?php
        } else {
          for ($j=0, $n2=sizeof($quotes[$i]['methods']); $j<$n2; $j++) {
// set the radio button to be checked if it is the method chosen
            $checked = (($quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id'] == $_SESSION['shipping']['id']) ? true : false);

            if ( ($checked == true) || ($n == 1 && $n2 == 1) ) {
              //echo '      <div id="defaultSelected" class="moduleRowSelected">' . "\n";
            //} else {
              //echo '      <div class="moduleRow">' . "\n";
            }
?>
<?php
            if ( ($n > 1) || ($n2 > 1) ) {
?>
<div class="important forward"><?php echo $currencies->format(zen_add_tax($quotes[$i]['methods'][$j]['cost'], (isset($quotes[$i]['tax']) ? $quotes[$i]['tax'] : 0))); ?></div>
<?php
            } else {
?>
<div class="important forward"><?php echo $currencies->format(zen_add_tax($quotes[$i]['methods'][$j]['cost'], $quotes[$i]['tax'])) . zen_draw_hidden_field('shipping', $quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id']); ?></div>
<?php
            }
?>

<?php echo zen_draw_radio_field('shipping', $quotes[$i]['id'] . '_' . $quotes[$i]['methods'][$j]['id'], $checked, 'id="ship-'.$quotes[$i]['id'] . '-' . str_replace(' ', '-', $quotes[$i]['methods'][$j]['id']) .'"'); ?>
<label for="ship-<?php echo $quotes[$i]['id'] . '-' . str_replace(' ', '-', $quotes[$i]['methods'][$j]['id']); ?>" class="checkboxLabel" ><?php echo $quotes[$i]['methods'][$j]['title']; ?></label>
<!--</div>-->
<br class="clearBoth" />
<?php
            $radio_buttons++;
          }
        }
?>

</fieldset>
<?php
    }
// eof: field set
      }
    }
?>

<?php
  } else {
?>
<h2 id="checkoutShippingHeadingMethod"><?php echo TITLE_NO_SHIPPING_AVAILABLE; ?></h2>
<div id="checkoutShippingContentChoose" class="important"><?php echo TEXT_NO_SHIPPING_AVAILABLE; ?></div>
<?php
  }
?>

<!-- bof Gift Wrap -->
<script language="javascript" type="text/javascript">
function showGWChoices(id, id2) { // This gets executed when the user clicks on the checkbox
	var obj = document.getElementById(id);
	var obj2 = document.getElementById(id2);
if (obj.checked) { 
	obj2.style.display= "block";
} else {
	obj2.style.display = "none";
}
}
</script>
<?php
   $value = "ot_giftwrap_checkout.php"; 
   include_once(zen_get_file_directory(DIR_WS_LANGUAGES . $_SESSION['language'] .
          '/modules/order_total/', $value, 'false'));
   include_once(DIR_WS_MODULES . "order_total/" . $value);
   $wrap_mod = new ot_giftwrap_checkout(); 
   $use_gift_wrap = true;
   if ($wrap_mod->check()) {
?>
      <br />
      <hr />
<fieldset class="shipping" id="gift_wrap">
<div><?php zen_draw_hidden_field('wrapconfig', $wrapconfig); ?></div>
<legend><?php echo GIFT_WRAP_HEADING; ?></legend>
<?php
    echo '<div id="cartWrapExplain">'; 
    echo '<a href="javascript:alert(\'' . GIFT_WRAP_EXPLAIN_DETAILS . '\')">' . GIFT_WRAP_EXPLAIN_LINK . '</a>';
    echo '</div>'; 
?>
      <table border="0" width="100%" cellspacing="0" cellpadding="0" id="cartContentsDisplay">
        <tr class="cartTableHeading">
        <th scope="col" id="ccProductsHeading"><?php echo TABLE_HEADING_PRODUCTS; ?></th>
         <th scope="col" id="ccWrapHeading"><?php echo GIFT_WRAP_CHECKOFF; ?></th>
        </tr>
<?php  
       // now loop thru all products to display quantity and price
   $prod_count = 1; 
   for ($i=0, $n=sizeof($order->products); $i<$n; $i++) {
     for ($q = 1; $q <= $order->products[$i]['qty']; $q++) {
        if ($prod_count%2 == 0) {
           echo '<tr class="rowEven">';
        } else {
           echo '<tr class="rowOdd">';
        }
        echo '<td class="cartProductDisplay">' . $order->products[$i]['name'];

        // if there are attributes, loop thru them and display one per line
        if (isset($order->products[$i]['attributes']) && sizeof($order->products[$i]['attributes']) > 0 ) {
            echo '<ul class="cartAttribsList">';
            for ($j=0, $n2=sizeof($order->products[$i]['attributes']); $j<$n2; $j++) {
                echo '<li>' . $order->products[$i]['attributes'][$j]['option'] . ': ' . nl2br($order->products[$i]['attributes'][$j]['value']) . '</li>'; 
            } // end loop
            echo '</ul>';
        } // endif attribute-info

        echo '</td>'; 
        // gift wrap setting
        echo '<td class="cartWrapCheckDisplay">'; 
        $prid = $order->products[$i]['id'];
        if (zen_get_products_virtual($order->products[$i]['id'])) {
           echo GIFT_WRAP_NA;
        } else if (DOWNLOAD_ENABLED && product_attributes_downloads_status($order->products[$i]['id'], $order->products[$i]['attributes'])) {
           echo GIFT_WRAP_NA; 
        } else if ($wrap_mod->exclude_product($prid)) {
           echo GIFT_WRAP_NA; 
        } else if ($wrap_mod->exclude_category($prid)) {
           echo GIFT_WRAP_NA; 
        } else { 
           $gift_id = "wrap_prod_" . $prod_count;
           echo zen_draw_checkbox_field($gift_id,'',false, 'id="'.$gift_id .'" onclick=\'showGWChoices("wrap_prod_' . $prod_count . '", "slider_' . $prod_count . '")\'');
        }
        echo "</td>"; 
        echo "</tr>"; 
        // Add in the wrapping paper images
        if ($prod_count%2 == 0) {
             echo '<tr class="rowEven" ';
        } else { 
             echo '<tr class="rowOdd" ';
        }
        echo ' id="wrap_sel_' . $prod_count . '">';
        echo '<td colspan="2" class="paperRow">';
        echo "\n"; 
        echo '<div id="slider_' . $prod_count . '" style="display:none;">' . "\n";
        $count = 0; 
        if ($wrapconfig == "Images") { 
           foreach ($papername as $paper) { 
              echo '<span class="wrapImageBox">';
              echo '<img src="' .GIFTWRAP_IMAGE_DIR . $paper . '" height="' . GIFTWRAP_IMAGE_HEIGHT . '" width="' . GIFTWRAP_IMAGE_WIDTH. '" />';
              echo zen_draw_radio_field('wrapping_paper_'.$prod_count, $paper, ($count==0));
              $count++; 
              echo '&nbsp;&nbsp;';
              echo '</span>';
           }
        } else if ($wrapconfig == "Descriptions") { 
           foreach ($wrap_selections as $paper) { 
              echo '<span class="wrapImageBox">';
              echo $paper; 
              echo zen_draw_radio_field('wrapping_paper_'.$prod_count, $paper, ($count==0));
              $count++; 
              echo '&nbsp;&nbsp;';
              echo '</span>';
           }
        }
        echo '</div>'; 
        echo '</td>'; 
        echo '</tr>'; 
        $prod_count++; 
     }
   }  // end for loopthru all products 
?>
      </table>
</fieldset>
      <hr />
      <br />
<?php
   }  
?>
<!-- eof Gift Wrap -->

<fieldset class="shipping" id="comments">
<legend><?php echo TABLE_HEADING_COMMENTS; ?></legend>
<?php echo zen_draw_textarea_field('comments', '45', '3'); ?>
</fieldset>

<div class="buttonRow forward"><?php echo zen_image_submit(BUTTON_IMAGE_CONTINUE_CHECKOUT, BUTTON_CONTINUE_ALT); ?></div>
<div class="buttonRow back"><?php echo '<strong>' . TITLE_CONTINUE_CHECKOUT_PROCEDURE . '</strong><br />' . TEXT_CONTINUE_CHECKOUT_PROCEDURE; ?></div>

</form>
</div>
