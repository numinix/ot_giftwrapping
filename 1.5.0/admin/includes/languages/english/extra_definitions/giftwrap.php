<?php
  // Special definitions for Gift Wrap Contribution
  define('GIFT_WRAP_SUMMARY_HEADING', 'Items to be Gift Wrapped'); 
  define('GIFT_WRAP_NO_TEXT', 'No gift wrapping selected'); 

  define('GIFT_WRAP_PAPER_SELECTED', 'Gift wrapping selected: '); 
  define('GIFT_WRAP_IMAGE_UNAVAILABLE', 'Gift wrap image unavailable; name is: '); 
?>
